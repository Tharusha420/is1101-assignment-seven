#include <stdio.h>
#define maxarraysize 50

int main(){
int a[maxarraysize][maxarraysize],b[maxarraysize][maxarraysize],product[maxarraysize][maxarraysize], add[maxarraysize][maxarraysize];
int arow, acolumn, brow, bcolumn;
int i,j,k;
int sum = 0;

printf ("Enter the number of rows and columns of matrix a:");
scanf ("%d %d",&arow, &acolumn);

printf ("Enter the elements of matrix a:\n");
for (i=0; i<arow; i++){
    for (j=0; j<acolumn; j++){
        scanf ("%d", &a[i][j]);
    }
}

printf ("Enter the number of rows and columns of matrix b:");
scanf ("%d %d", &brow, &bcolumn);

if (brow != acolumn){
    printf ("Error! Cannot multiply different size matrices");

}
else {
    printf ("Enter the elements of matrix b: \n");
    for (i=0; i<brow; i++){
        for (j=0; j<bcolumn; j++){
            scanf ("%d", &b[i][j]);
        }
    }

}
printf ("\n");
for (i=0; i<arow; i++){
    for (j=0; j<bcolumn; j++){
        for (k=0; k<brow; k++){
            sum += a[i][k] * b[k][j];
        }
        product[i][j]= sum;
        sum=0;
    }
}
printf ("Result matrix multiplicaton\n");
for (i=0; i<arow; i++){
    for (j=0; j<bcolumn; j++){
        printf ("%d ", product [i][j]);
    }
    printf("\n");
}

for (i=0; i<arow; i++){
    for (j=0; j<bcolumn; j++){
        add [i][j]=a[i][j] + b[i][j];
    }
}
printf ("\nResult matrix addition\n");
for (i=0; i<arow; i++){
    for (j=0; j<bcolumn; j++){
        printf ("%d ", add[i][j]);
    }
    printf("\n");
}

return 0;

}
